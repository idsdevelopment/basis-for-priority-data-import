﻿using System;
using System.Collections.Generic;
using System.Net;
using GetTripsFromIDS.ServiceReference;

namespace GetTripsFromIDS
{
    internal class Account
    {
        internal readonly remoteAccountValue RemoteAccount;
        internal readonly remoteAddress Address;

        internal Account( remoteAccountValue remoteAccount, remoteAddress address )
        {
            RemoteAccount = remoteAccount;
            Address = address;
        }
    }

    internal class Trip
    {
        internal readonly remoteTrip RemoteTrip;
        internal readonly remoteAddress PickupAddress, DeliveryAddress;

        internal Trip( remoteTrip remoteTrip, remoteAddress pickupAddress, remoteAddress deliveryAddress )
        {
            RemoteTrip = remoteTrip;
            PickupAddress = pickupAddress;
            DeliveryAddress = deliveryAddress;
        }
    }

    internal class TripsWithinAccount
    {
        internal readonly Account Account;
        internal readonly List<Trip> Trips;

        internal TripsWithinAccount( Account account, List<Trip> trips )
        {
            Account = account;
            Trips = trips;
        }
    }


    internal class TripsWithinAccountList : List<TripsWithinAccount>
    {
    }


    // Take too long to get addresses
    internal class AddressCache : Dictionary<string, remoteAddress>
    {
        internal bool TryGetAddress( string addressId, out remoteAddress address )
        {
            lock( this )
                return TryGetValue( addressId, out address );
        }

        internal void AddAddress( string addressId, remoteAddress address )
        {
            lock( this )
            {
                try
                {
                    Add( addressId, address );
                }
                catch
                {
                }
            }
        }
    }


    internal class Program
    {
        private const string DEDUG_SERVER = "/D",
                             MAIN_SERVER = "/M",
                             COMPANY_ID = "/C=",
                             USER_ID = "/U=",
                             PASSWORD_ID = "/P=",
                             FROM_DATE = "/FD=",
                             TO_DATE = "/TD=";

        private static volatile bool Error;

        private static readonly AddressCache AddressCache = new AddressCache();

        internal static remoteAddress GetAddress( Client client, string addressId )
        {
            remoteAddress Addr;

            if( !AddressCache.TryGetAddress( addressId, out Addr ) )
            {
                Addr = client.GetAddressById( addressId );
                AddressCache.AddAddress( addressId, Addr );
            }
            return Addr;
        }

        private static void Main( string[] args )
        {
            ServicePointManager.DefaultConnectionLimit = 64;

            string CompanyId = "",
                   UserId = "",
                   Password = "",
                   FromDate = "",
                   ToDate = "";


            var HaveServer = false;

            foreach( var Arg in args )
            {
                var A = Arg.Trim();
                switch( A )
                {
                case DEDUG_SERVER:
                    EndPoints.UseTestingServer = true;
                    HaveServer = true;
                    break;
                case MAIN_SERVER:
                    HaveServer = true;
                    break;
                default:
                    if( A.StartsWith( COMPANY_ID ) )
                        CompanyId = A.Remove( 0, COMPANY_ID.Length ).Trim();
                    else if( A.StartsWith( USER_ID ) )
                        UserId = A.Remove( 0, USER_ID.Length ).Trim();
                    else if( A.StartsWith( PASSWORD_ID ) )
                        Password = A.Remove( 0, PASSWORD_ID.Length ).Trim();
                    else if( A.StartsWith( FROM_DATE ) )
                        FromDate = A.Remove( 0, FROM_DATE.Length ).Trim();
                    else if( A.StartsWith( TO_DATE ) )
                        ToDate = A.Remove( 0, TO_DATE.Length ).Trim();
                    break;
                }
            }

            Environment.ExitCode = 0;

            try
            {
                if( !HaveServer )
                    throw new Exception( "No server specified" );

                if( string.IsNullOrWhiteSpace( CompanyId ) )
                    throw new Exception( "No comapny specified" );

                if( string.IsNullOrWhiteSpace( UserId ) )
                    throw new Exception( "No user specified" );

                if( string.IsNullOrWhiteSpace( Password ) )
                    throw new Exception( "No password specified" );

                if( string.IsNullOrWhiteSpace( FromDate ) )
                    throw new Exception( "No from date specified" );

                DateTime FDateTime;
                if( !DateTime.TryParse( FromDate, out FDateTime ) )
                    throw new Exception( "Invalid from date specified" );

                if( string.IsNullOrWhiteSpace( ToDate ) )
                    throw new Exception( "No to date specified" );

                DateTime TDateTime;
                if( !DateTime.TryParse( ToDate, out TDateTime ) )
                    throw new Exception( "Invalid to date specified" );

                TDateTime = TDateTime.AddHours( 23 ).AddMinutes( 59 ).AddSeconds( 59 ).AddMilliseconds( 999 );

                List<remoteAccountValue> Accounts;

                Console.WriteLine( "Getting Accounts" );
                using( var Client = new Client( CompanyId, UserId, Password ) )
                    Accounts = Client.GetAccounts();

                var TripsByAccount = new TripsWithinAccountList();


                Console.WriteLine( "Getting Trips & Addresses" );
                new Tasks.Task<remoteAccountValue>().Run( Accounts, account =>
                {
                    if( !Error )
                    {
                        try
                        {
                            remoteAddress Address;
                            List<remoteTrip> RemoteTrips;

                            using( var Client = new Client( CompanyId, UserId, Password ) )
                            {
                                Address = Client.GetAddressById( account.addressId );
                                RemoteTrips = Client.GetTripsForAccountByDate( account.accountId, FDateTime, TDateTime );
                            }

                            var Trips = new List<Trip>();

                            new Tasks.Task<remoteTrip>().Run( RemoteTrips, trip =>
                            {
                                using( var Client = new Client( CompanyId, UserId, Password ) )
                                {
                                    var Pu = GetAddress( Client, trip.pickupAddressId );
                                    var Del = GetAddress( Client, trip.deliveryAddressId );
                                    lock( Trips )
                                        Trips.Add( new Trip( trip, Pu, Del ) );
                                }
                            }, 5 );

                            lock( TripsByAccount )
                                TripsByAccount.Add( new TripsWithinAccount( new Account( account, Address ), Trips ) );
                        }
                        catch( Exception E )
                        {
                            Console.WriteLine( E );
                            Error = true;
                        }
                    }
                } );

                if( !Error )
                {
                    //
                    // Your Code Here 
                    // TripsByAccount Has all The Info
                    //
                }
                else
                    Environment.ExitCode = 100;
            }
            catch( Exception E )
            {
                Console.WriteLine( E );
                Environment.ExitCode = 100;
            }
        }
    }
}