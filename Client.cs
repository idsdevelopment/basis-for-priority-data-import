﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using GetTripsFromIDS.ServiceReference;

namespace GetTripsFromIDS
{
    public static class EndPoints
    {
        public const string JBTEST = "http://198.50.187.104:8080/ids-beans/IDSWS",
                            APP = "http://71.19.250.132:8080/ids-beans/IDSWS";

        public static bool UseTestingServer;
    }

    internal class Client : DSWSClient
    {
        internal enum STATUS
        {
            UNSET,
            NEW,
            ACTIVE,
            DISPATCHED,
            PICKED_UP,
            DELIVERED,
            VERIFIED,
            POSTED,
            DELETED,
            SCHEDULED,
            UNDELIVERED = -3
        }

        private const int TIMEOUT = 15;

        private readonly string AuthToken;

        internal Client( string carrierId, string userName, string password )
        {
            AuthToken = $"{carrierId}-{userName}-{password}";

            InnerChannel.OperationTimeout = TimeSpan.FromMinutes( TIMEOUT );
            Endpoint.Address = new EndpointAddress( EndPoints.UseTestingServer ? EndPoints.JBTEST : EndPoints.APP );
        }

        internal List<remoteAccountValue> GetAccounts()
        {
            return findAllAccountsForCarrier( new findAllAccountsForCarrier { authToken = AuthToken } ).ToList();
        }

        internal remoteAddress GetAddressById( string id )
        {
            return findAddress( new findAddress { authToken = AuthToken, addressId = id } )?.@return;
        }

        internal List<remoteTrip> GetTripsForAccountByDate( string accountId, DateTime fromDate, DateTime toDate, STATUS fromStatus = STATUS.VERIFIED, STATUS toStatus = STATUS.POSTED )
        {
            var Request = new findTripsForAccountByDate { authToken = AuthToken, accountId = accountId, from = fromDate, fromSpecified = true, to = toDate, toSpecified = true, lowerStatus = (int)fromStatus, upperStatus = (int)toStatus };
            return findTripsForAccountByDate( Request ).ToList();
        }
    }
}